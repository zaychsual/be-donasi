<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## REQUIREMENT

APLIKASI INI BUAT REFRESH OTAK GW NGERJAIN API LARAVEL,FE NYA PAKE VUE JS

- PHP VERSION ^ 8
- DATABASE MYSQL

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## INSTALLATION STEP
- clone applikasi
 ```
 $ git clone
```
- install composer
```
$ composer install
```
- update composer
```
$ composer dump-autoload
```
- install package tailwind
```
$ npm install
```
- compile tailwind
```
$ npm run build
```
- aktifkan laravel paging
```
$ php artisan vendor:publish --tag=laravel-pagination
```
