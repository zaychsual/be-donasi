<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

//group route with prefix "admin"
Route::prefix('admin')->group(function () {
    //group route with middleware "auth"
    Route::group(['middleware' => 'auth'], function() {

        //admin only
        Route::group(['as' => 'admin.'], function() {
            Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
            Route::resource('/category', CategoryController::class);
            Route::resource('/campaign', CampaignController::class);
            //route donatur
            Route::get('/donatur', [DonaturController::class, 'index'])->name('donatur.index');
            //route donation
            Route::get('/donation', [DonationController::class, 'index'])->name('donation.index');
            Route::get('/donation/filter', [DonationController::class, 'filter'])->name('donation.filter');
            //route profile
            Route::get('/profile', [ProfileController::class, 'index'])->name('profile.index');
            //route resource slider
            Route::resource('/slider', SliderController::class, ['except' => ['show', 'create', 'edit', 'update']]);
        });
    });
});
